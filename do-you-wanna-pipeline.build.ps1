﻿#Requires -modules InvokeBuild
#Requires -modules Pester
#Requires -modules platyPS
#Requires -modules posh-git
#Requires -modules Assert
#Requires -modules PSScriptAnalyzer
[CmdletBinding()]
#DefaultParameterSetName="IncrementVersionBuild")]
param(
[String]$Message,
[string]$ApiKey = (Get-Content ..\git\PSGallery.txt),

#[Parameter(ParameterSetName='IncrementVersionRevision')]
[switch]$IncrementVersionRevision,
#[Parameter(ParameterSetName='IncrementVersionMinor')]
[switch]$IncrementVersionMinor,
#[Parameter(ParameterSetName='IncrementVersionMajor')]
[switch]$IncrementVersionMajor,
[switch]$IncrementVersionReset
)

Add-BuildTask Clean-Workspace {
	Remove-Item .\$ModuleName -Recurse -ErrorAction SilentlyContinue
	git add --update
	git clean -xdf
}

Add-BuildTask Increment-Version Get-ProjectSettings, {
	if ($IncrementVersionMajor) {
		[version]$ProjectSettings['ModuleVersion'] = "{0}.0.0.0" -f ($ProjectSettings['ModuleVersion'].Major + 1)
	}
	elseif ($IncrementVersionMinor) {
		[version]$ProjectSettings['ModuleVersion'] = "{0}.{1}.0.0" -f ($ProjectSettings['ModuleVersion'].Major),
		($ProjectSettings['ModuleVersion'].Minor + 1)
	}
	elseif ($IncrementVersionRevision) {
		[version]$ProjectSettings['ModuleVersion'] = "{0}.{1}.{2}.{3}" -f $ProjectSettings['ModuleVersion'].Major,
		$ProjectSettings['ModuleVersion'].Minor,
		$ProjectSettings['ModuleVersion'].Build,
		$ProjectSettings['ModuleVersion'].Revision + 1
	}
	elseif ($IncrementVersionReset) {
		[version]$ProjectSettings['ModuleVersion'] = "0.0.1.0"
	}
	else {
		[Version]$ProjectSettings['ModuleVersion'] = "{0}.{1}.{2}.0" -f $ProjectSettings['ModuleVersion'].Major,
		$ProjectSettings['ModuleVersion'].Minor,
		($ProjectSettings['ModuleVersion'].Build + 1)
	}
}

Add-BuildTask Update-ProjectSettings Get-ProjectSettings, {
	$TextMask = '    {0} = [{1}]"{2}"'

	'$ProjectSettings = @{' | Out-File -Encoding UTF8 $SettingsPath
	$ProjectSettings.GetEnumerator() | Foreach-Object {
		$TextMask -f $PSItem.Name,$PSItem.Value.GetType(),$PSItem.Value |
		Out-File -Encoding UTF8 $SettingsPath -Append
	}
	'}' | Out-File -Encoding UTF8 $SettingsPath -Append

	git add --update
}


Add-BuildTask Init {
	$Script:ModuleName = $PSScriptRoot | Split-Path -Leaf
	$Script:ModuleFilePath = ".\$ModuleName\$ModuleName.psm1"
	$Script:SettingsPath = "$PSScriptRoot\$($PSScriptRoot | Split-Path -Leaf).Settings.ps1"
},
Clean-Workspace, {


	# Create temp dir for files to be copied to.
	New-Item -ItemType Directory -Name $ModuleName

}

Add-BuildTask Get-ProjectSettings Init, {
	. $SettingsPath
	$Script:DefaultSettings = [ordered]@{
		Path = ".\$ModuleName\$ModuleName.psd1"
		ModuleVersion = "0.0.1.0"
		Guid = (New-Guid).Guid
		Author = "Devin Rich"
		CompanyName = "Me"
		Description = "A module to make replacing text easier"
		RootModule = "$ModuleName.psm1"
		PowerShellVersion = "3.0"
		FormatsToProcess = ""
		FunctionsToExport = ""
		CmdletsToExport = ""
		VariablesToExport = ""
		AliasesToExport = ""
		Tags = ""
	}

	foreach ($Key in $ProjectSettings.GetEnumerator())
	{
		$DefaultSettings[$Key.Name] = $Key.Value
	}

	$Script:ProjectSettings = $DefaultSettings
}

Add-BuildTask Test Init, {
	$MissedCommands = Invoke-Pester .\Tests\ -CodeCoverage Public\*,Private\* -PassThru
	if ($MissedCommands.CodeCoverage.MissedCommands.Count) {
		throw "Not all lines were covered by unit testing. Missed command count: $MissedCommands"
	}

	# Todo:
	# This is split into 2 tasks to allow uploading the NUnitXml file to AppVeyor

}

Add-BuildTask Analyze Init, {
	Invoke-ScriptAnalyzer -Path *.ps1 -Recurse -IncludeDefaultRules -Fix
}

Add-BuildTask Build Init, Test, Analyze, {
	Get-ChildItem .\Public\*.ps1 | Foreach-Object {
		Get-Content $PSItem.FullName |
		Out-File $ModuleFilePath -Append
	}

	$ProjectSettings['FunctionsToExport'] = Get-ChildItem .\Public\*.ps1 | Select-Object -expand Basename
	New-ModuleManifest @ProjectSettings

}

Add-BuildTask Validate-Documentation Build, {
	# Undocumented commands and parameters will result in a line with the following text on their line:
	# {{Fill InputObject Description}}
	# Check docs folder and see if any files contain {{. Fail if so.
	Import-Module $ModuleFilePath -Force
	New-MarkdownHelp -Module ($PSScriptRoot | Split-Path -Leaf) -OutputFolder .\docs -Force
	Get-ChildItem .\Docs\* | Foreach-Object {
		Get-Content $PSItem.FullName |
		Foreach-Object {
			if ($PSItem -like "{{Fill*") {
				Throw "Help not complete. Missing required documentation begins on line $($PSItem.ReadCount)"
			}
		}
	}
	#Don't know why I need external help. But, I have an XML file I could use if this were binary I guess?
	New-ExternalHelp .\docs -OutputPath en-US\ -Force
}

Add-BuildTask -if {$Message} Publish Increment-Version, Update-ProjectSettings, Build, Validate-Documentation, {
	git add .\$ModuleName
	git clean -xdf
	git commit -m $Message
	git push

	$Params = @{
		NuGetApiKey = $ApiKey
		Path = ".\do-you-wanna-pipeline\"
		#Tags = "Console"
		ProjectUri = "https://gitlab.com/devirich/do-you-wanna-pipeline"
		ReleaseNotes = $Message
		}
		Publish-Module @Params

	<#

		param(
		[Parameter(Mandatory=$True)]
		[String]$Message,
		$ErrorActionPreference = "Stop",

		)




	#>

}

Add-BuildTask Template {
	# Add in dynamic template?
	# Not currently.
}


# Synopsis: Build and clean.
Add-BuildTask . Increment-Version,
Update-ProjectSettings,
Validate-Documentation,
Publish
