$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$sut = (Split-Path -Leaf $MyInvocation.MyCommand.Path).replace('.Tests.','.')
. "$here\..\*\$sut"

Describe "Get-ObjectProperties" {
	It "in and out" {
		[pscustomobject]@{a=1} | Get-ObjectProperties |
		Should Be "a"
	}
}