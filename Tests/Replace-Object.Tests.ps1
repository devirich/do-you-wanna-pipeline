$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$sut = (Split-Path -Leaf $MyInvocation.MyCommand.Path).replace('.Tests.','.')
. "$here\..\*\$sut"

Describe "Replace-Object" {
	It "strings in, strings out" {
		2,3 | Replace-Object -pattern 2,"123" | 
		Should Be @(123,3)
	}
	It "strings in, replace out" {
		4,2,3 | Replace-Object -pattern 2 | 
		Should Be @(4,"",3)
	}
	It "Object in, single property replace" {
		[hashtable]$Results = @{
			Actual   = [pscustomobject]@{a=1;b=123} | 
			Replace-Object -Pattern "\d" -Replacement "a" -Property a
			
			Expected = ([pscustomobject]@{a='a';b=123})
		}
		Assert-Equivalent @Results
	}
	It "Object, multiple properties" {
		[hashtable]$Results = @{
			Actual   = [pscustomobject]@{a=1;b=123} | 
			Replace-Object "\d","a" -Property a,b
			
			Expected = ([pscustomobject]@{a='a';b='aaa'})
		}
		Assert-Equivalent @Results
		
	}
	It "Object, wildcard (all) properties - low priority" {
		#[hashtable]$Results = @{
		#	Actual   = [pscustomobject]@{a=1;b=123} | 
		#	Replace-Object "\d","a" -Property *
		#	
		#	Expected = ([pscustomobject]@{a='a';b='aaa'})
		#}
		#Assert-Equivalent @Results
	}
	It "Object, hashtable multiple properties" {
		[hashtable]$Results = @{
			Actual   = [pscustomobject]@{a=1;b=123} | 
			Replace-Object -MultiReplace @{a="\d","DIGIT"; b="2(?=(.))"}
			
			Expected = ([pscustomobject]@{a='DIGIT';b=13})
		}
		Assert-Equivalent @Results
	}
	It "Object, hashtables multiple properties (repeated of at least 1 prop)" {
		[hashtable]$Results = @{
			Actual   = [pscustomobject]@{a=1;b=123} | 
			Replace-Object -MultiReplace @{"b"=2,'ELEET'; a="\d","DIGIT"},@{'a'='[a-g]',"_"}
			
			Expected = ([pscustomobject]@{a='_I_IT';b='1ELEET3'})
		}
		Assert-Equivalent @Results
	}
}