function Get-ObjectProperties
{
	<#
		.SYNOPSIS
		Placeholder for private functions area
		
		
		#>
	
	Param(
	[Parameter(
	Position=0,
	Mandatory=$True,
	ValueFromPipeline=$True)]
	$InputObject
	)
	Begin {}
	Process {
		$InputObject.PSObject.Properties.Name
	}
	End {}
	
}
