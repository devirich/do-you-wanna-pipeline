Set-Alias -Name Replace -Value Replace-Object
function Replace-Object {
	<#
		.SYNOPSIS
		Object based regex replacer
		
		.DESCRIPTION
		Replace-Object lets you update one or more properties in an input object
		with the desired regular expressions. This is to save you from having to
		do long calculated properies if you just want to do a quick regex replace.
		Also lets you replace on primitive input types
		
		
		.PARAMETER InputObject
		The object(s) to be operated on.
		
		.PARAMETER Property
		Description of each of the parameters
		
		.PARAMETER Pattern
		Description of each of the parameters
		
		.PARAMETER Replacement
		Description of each of the parameters
		
		.PARAMETER MultiReplace
		Description of each of the parameters
		
		.INPUTS
		Accepts primitive objects like strings and psobjects
		
		.OUTPUTS
		Outputs the same object type
		
		.EXAMPLE
		> "Foo","Bot","Cat" | Replace o e
		> "Foo","Bot","Cat" | Replace o,e
		Fee
		Bet
		Cat
		
		Using alias and positions for shorthand use (2 examples in 1!)
		
		.EXAMPLE
		"Foo","Bar","Cat" | Replace-Object -Regex "a.$" -RegexReplacement "am"
		Foo
		Bam
		Cam
		
		Using pipeline on primitive objects (strings)
		
		.EXAMPLE
		Replace-Object -InputObject ([pscustomobject]@{a=3;b=42}) -Property a -Regex "$" -RegexReplacement "a"
		a   b
		-   -
		3a 42
		
		No pipeline, single property regex replacement
		
		.EXAMPLE
		Replace-Object -InputObject ([pscustomobject]@{a=3;b=42}) -Property b -Regex "^."
		a b
		- -
		3 2
		
		
		
		.EXAMPLE
		[pscustomobject]@{a=3;b=42} | Replace-Object -HashTable @{"b"=4,1337; a="\d","BAR"}
		a   b
		-   -
		BAR 13372
		
		.LINK
		
		
		.NOTES
		
		
	#>
	
	Param(
	[Parameter(
	#Position=0,
	Mandatory=$True,
	ValueFromPipeline=$True)]
	$InputObject,
	
	[Parameter(
	ParameterSetName='Simple',
	Position=0,
	Mandatory=$True)]
	[String[]]$Pattern,
	
	[Parameter(
	ParameterSetName='Simple',
	Position=1)]
	[String]$Replacement,
	
	[Parameter(
	ParameterSetName='Simple',
	Position=2)]
	[String[]]$Property,
	
	[Parameter(
	ParameterSetName='Hashtable',
	Position=1,
	Mandatory=$False)]
	[Hashtable[]]$MultiReplace
	)
	
	Begin {
		$WorkerHT = [ordered]@{}
		if ($MultiReplace)
		{
			foreach ($HT in $MultiReplace)
			{
				foreach ($Entry in $HT.GetEnumerator())
				{
					$i++
					# We call [regex]::Replace() later that requires 3 params. Add a dummy 3rd if not given
					if ($Entry.Value.Count -eq 1)
					{
						$WorkerHT["$i"] = $Entry.Name,$Entry.Value[0],''
					} 
					else {
						$WorkerHT["$i"] = $Entry.Name,$Entry.Value[0],$Entry.Value[1]
					}
					
				}
			}
		}
		else
		{
			if (!$property) {
				$property = ""
			}
			elseif ($Property -eq "*")
			{ #This won't work because in the begin, I don't know what the objects in the pipe are like yet.
			}
			
			foreach ($PropertyName in $Property)
			{
				$i++
				if ($Pattern.Count -gt 1) {
					$WorkerHT["$i"] = $PropertyName,$Pattern[0],$Pattern[1]
				}
				elseif ($Pattern.count -eq 1 -and $Replacement) {
				$WorkerHT["$i"] = $PropertyName,$Pattern[0],$Replacement
				#$MultiReplace = @{$property = $Pattern,$Replacement}
				}
				elseif ($Pattern.count -eq 1 -and !$Replacement) {
				$WorkerHT["$i"] = $PropertyName,$Pattern[0],''
				#$MultiReplace = @{$property = $Pattern,''}
				}
				}
				}
				}
				
				Process {
				Foreach ($Object in $InputObject)
				{
				foreach ($Entry in $WorkerHT.GetEnumerator())
				{
				If (!$Entry.Value[0])
				{
				$Object = [regex]::replace($Object,$Entry.value[1],$Entry.value[2],1)
				}
				else
				{
				$Object.($Entry.Value[0]) = [regex]::replace(
				$Object.($Entry.Value[0]),
				$Entry.value[1],
				$Entry.value[2],
				1 # https://docs.microsoft.com/en-us/dotnet/api/system.text.regularexpressions.regexoptions
				)
				}
				}
				}
				$Object
				}
				End {
				
				}
				}									