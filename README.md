# Do you wanna pipeline

A powershell module to bring pipeline support to some of your favorite operators and commands. 
Provides some extra functionality too. This includes the ability to run a regex replace on 
multiple properties. Or to specify arbitrary replace operations on any objects and optionally,
repeatedly on a single object. 

```
.EXAMPLE
> "Foo","Bar","Cat" | Replace-Object -Regex "a.$" -RegexReplacement "am"
Foo
Bam
Cam

Using pipeline on primitive objects (strings)

.EXAMPLE
> "Foo","Bot","Cat" | Replace o e
> "Foo","Bot","Cat" | Replace o,e
Fee
Bet
Cat

Using alias and positions for shorthand use (2 examples in 1!)

.EXAMPLE
> Replace-Object -InputObject ([pscustomobject]@{a=3;b=42}) -Property a -Regex "$" -RegexReplacement "a"
a   b
-   -
3a 42

No pipeline, single property regex replacement

.EXAMPLE
> Replace-Object -InputObject ([pscustomobject]@{a=3;b=42}) -Property b -Regex "^."
a b
- -
3 2

.EXAMPLE
> [pscustomobject]@{a=3;b=42} | Replace-Object -HashTable @{"b"=4,1337; a="\d","BAR"}
a   b
-   -
BAR 13372
```